import TodoList from './components/TodoList.mjs'

export default {
	components: {
		TodoList
	},
	template: `
	<div id="app">
		<h1>My Todo App!</h1>
		<TodoList/>
	</div>
	`
}