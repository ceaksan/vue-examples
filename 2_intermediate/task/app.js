Vue.component("tasks-comp", {
  created() {
    this.taskOrderBy("priority");
  },
  props: {
    search: {},
    title: {
      required: true,
      type: String
    },
    slug: {
      required: true,
      type: String
    }
  },
  data() {
    return {
      taskData: this.$root[this.slug],
      filteredTasks: [],
      searchedTask: null
    };
  },
  template: `
  <div class="container" v-if="filteredTasks.length > 0">
    <transition name="fade"><div class="box" v-if="this.searchedTask">Aranan Kelime: <span class="tag is-light is-medium">{{ this.searchedTask }}</span></div></transition>
  
    <h2 class="title is-6" :title="this.title">{{ this.title.toUpperCase() }} <span class="tag is-link" v-text="filteredTasks.length"></span></h2>
    <h4 class="title is-6"><a class="is-link" @click="taskOrderBy('id')">Due Date</a> - <a class="is-link" @click="taskOrderBy('priority')">Priority</a></h4>
    <div class="columns is-12 is-multiline">
      <task-comp v-for="(task, index) in searchBy(search)" :key="slug + '-' + index" :task="task" :slug="slug" :index="index"></task-comp>
    </div>
    <hr />
  </div>
  `,
  methods: {
    taskOrderBy(type) {
      const sortCondition =
        type === "id" ?
        (x, y) => x.id - y.id :
        (x, y) => x.priority - y.priority;
      this.filteredTasks = this.taskData
        .slice()
        .sort(sortCondition)
        .reverse();
    },
    searchBy(val) {
      this.searchedTask = val;
      if (!this.searchedTask) return this.filteredTasks;
      let fTask = this.searchedTask.toLowerCase()

      return this.filteredTasks.filter(t => {
        return t.title.toLowerCase().includes(fTask) || t.description.toLowerCase().includes(fTask)
      })

    }
  },
  watch: {
    taskData: function (val) {
      this.filteredTasks = val;
    }
  }
});

Vue.component("task-comp", {
  props: {
    task: {
      required: true,
      type: Object,
      default: () => ({})
    },
    index: {
      required: true,
      type: Number
    },
    slug: {
      required: true,
      type: String
    }
  },
  data() {
    return {};
  },
  template: `
  <transition name="fade">
  <div class="column is-3">
		<div class="card has-equal-height">
			<header class="card-header"><h1 class="card-header-title">{{ task.title }}</h1></header>
      <div class="card-content">
        <div class="content">
          <p><span :class="['tag', 'is-info', this.$root.priorities[task.priority-1] ]">Öncelik: {{ task.priority }}</span><span class="tag">Tarih: <time :datetime="task.date">{{ task.date }}</time></span></p>
          <p v-html="task.description"></p>
				</div>
      </div>
      <component is="task-footer" v-on:setcompleted="taskcompleted(index, slug)" v-on:setinprogress="taskinprogress(index, slug)" :key="'button-' + index" :index="index" :slug="slug"></component>
		</div>
  </div>
  </transition>
  `,
  methods: {
    taskcompleted(val, tasktype) {
      if (tasktype == "taskactive") {
        const task = this.$root["taskactive"].splice(val, 1);
        this.$root["taskcompleted"].push(task[0]);
      } else if (tasktype == "taskcompleted") {
        const task = this.$root["taskcompleted"].splice(val, 1);
        this.$root["taskinprogress"].push(task[0]);
      } else if (tasktype == "taskinprogress") {
        const task = this.$root["taskinprogress"].splice(val, 1);
        this.$root["taskcompleted"].push(task[0]);
      } else {
        console.log('Error!')
      }
    },
    taskinprogress(val, tasktype) {
      if (tasktype == "taskactive") {
        const task = this.$root["taskactive"].splice(val, 1);
        this.$root["taskinprogress"].push(task[0]);
      } else if (tasktype == "taskcompleted") {
        const task = this.$root["taskcompleted"].splice(val, 1);
        this.$root["taskinprogress"].push(task[0]);
      } else if (tasktype == "taskinprogress") {
        const task = this.$root["taskinprogress"].splice(val, 1);
        this.$root["taskcompleted"].push(task[0]);
      } else {
        console.log('Error!')
      }
    }
  }
});

Vue.component("task-footer", {
  props: {
    slug: {
      required: true,
      type: String
    }
  },
  data() {
    return {};
  },
  template: `
  <footer class="card-footer">
    <div class="buttons has-addons">
      <a v-if="slug !== 'taskcompleted'" :class="['card-footer-item', 'button', 'is-link']" v-on:click="$emit('setcompleted')">Completed</a>
      <a v-if="slug !== 'taskinprogress'" :class="['card-footer-item', 'button', 'is-warning']" v-on:click="$emit('setinprogress')">In Progress</a>
		</div>
	</footer>
  `
});

var app = new Vue({
  el: "#app",
  data: {
    tasks: [{
        title: "Active Tasks",
        slug: "taskactive"
      },
      {
        title: "In Progress",
        slug: "taskinprogress"
      },
      {
        title: "Completed Tasks",
        slug: "taskcompleted"
      }
    ],
    modelActive: false,
    priorities: [
      "is-primary",
      "is-info",
      "is-success",
      "is-warning",
      "is-danger"
    ],
    newtask: {
      id: null,
      title: null,
      priority: 1,
      description: null,
      date: null
    },
    "taskactive": [],
    "taskinprogress": [],
    "taskcompleted": [],
    searchQuery: null
  },
  methods: {
    submitnewtask() {
      // this.$root["taskactive"].push(this.newtask);
      let data = this.newtask
      axios
        .put("https://vuetest.free.beeceptor.com/add", {
          data: {
            tasks: [data]
          }
        })
        .then(r => console.log(r.status))
        .catch(e => console.log(e));
      this.$root.taskactive = [];
      this.getTaskFromAPI();
      this.newtask = [];
    },
    getTaskFromAPI() {
      axios({
        method: "get",
        url: "https://vuetest.free.beeceptor.com",
      }).then((resp => {
        this.$root.taskactive.push(...resp.data.data.tasks);
      })).catch(error => console.log(error))
    }
  },
  computed: {
    getDate() {
      let getCurrentDate = new Date().toISOString().split("T")[0];
      return getCurrentDate;
    },
    getNewID() {
      let getLastestID = this.$root["taskactive"].length;
      return getLastestID;
    }
  },
  created() {
    this.newtask.date = this.getDate;
    this.newtask.id = this.getNewID;
    this.getTaskFromAPI();
  },
  watch: {}
});