<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Vue + PHP</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.min.css" />
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
  </head>
  <body>
  <div id="app">

    <section class="section">
      <div class="container">
       
        <nav class="navbar" role="navigation" aria-label="dropdown navigation">
          <h1 class="title"><a href="/" v-text="maintitle"></a></h1>
          <div class="navbar-end">
            <div class="navbar-item has-dropdown is-hoverable">
              <a class="navbar-link">Menu</a>
              <div class="navbar-dropdown">>Go to Home</router-link>
                <router-link class="navbar-item" to="/foo">Go to Foo</router-link>
                <router-link class="navbar-item" to="/bar">Go to Bar</router-link>
                <hr class="navbar-divider">
                <a class="navbar-item" @click="getMessage()">Get new one!</a>
              </div>
            </div>
          </div>
        </nav>

      </div>
    </section>

      <keep-alive>
        <router-view :title="title" :subtitle="subtitle" :passage="passage"></router-view>
      </keep-alive>

  </div>

  <script src="https://unpkg.com/vue/dist/vue.js"></script>
  <script src="https://unpkg.com/vue-router/dist/vue-router.js"></script>
  <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

  <script>
    const Section = {
      props: ['title', 'subtitle', 'passage'],
      template: `
        <div>
          <section class="hero is-light">
            <div class="hero-body">
              <div class="container">
                <h2 class="title">{{ this.title }}</h2>
                <h3 class="subtitle">{{ this.subtitle }}</h3>
              </div>
            </div>
          </section>
          
          <section class="section">
            <div class="container">
              <p>{{ this.passage }}</p>
            </div>
          </section>
        </div>
      `
    };

    const Default = {
      template: `<component-section :title="title" :subtitle="subtitle" :passage="passage" />`,
      components: {
        'component-section': Section
      },
      data() {
        return {
          title: 'Nineteen Eighty-Four',
          subtitle: 'by George Orwell',
          passage: 'The Ministry of Truth, which concerned itself with news, entertainment, education and the fine arts. The Ministry of Peace, which concerned itself with war. The Ministry of Love, which maintained law and order. And the Ministry of Plenty, which was responsible for economic affairs. Their names, in Newspeak: Minitrue, Minipax, Miniluv and Miniplenty.'
        }
      }
    };

    const Foo = { 
      template: `<component-section :title="title" :subtitle="subtitle" :passage="passage" />`,
      components: {
        'component-section': Section
      },
      data() {
        return {
          title: 'In Search of Lost Time',
          subtitle: 'Marcel Proust',
          passage: 'We believe that we can change the things around us in accordance with our desires—we believe it because otherwise we can see no favourable outcome. We do not think of the outcome which generally comes to pass and is also favourable: we do not succeed in changing things in accordance with our desires, but gradually our desires change. The situation that we hoped to change because it was intolerable becomes unimportant to us. We have failed to surmount the obstacle, as we were absolutely determined to do, but life has taken us round it, led us beyond it, and then if we turn round to gaze into the distance of the past, we can barely see it, so imperceptible has it become.'
        }
      }
    };

    const Bar = { 
      template: `<component-section :title="title" :subtitle="subtitle" :passage="passage" />`,
      components: {
        'component-section': Section
      },
      data() {
        return {
          title: 'Death of a Salesman',
          subtitle: 'Arthur Miller',
          passage: 'When today fails to offer the justification for hope, tomorrow becomes the only grail worth pursuing.'
        }
      }
    };

    const routes = [
        { path: '/foo', component: Foo },
        { path: '/bar', component: Bar },
        { path: '*', component: Default },
    ];

    const router = new VueRouter({
        mode: 'history',
        routes: routes
    });

    var app = new Vue({
        el: "#app",
        router: router,
        data() {
          return {
            maintitle: 'Classic Novels',
            title: null,
            subtitle: null,
            passage: null,
            showmodal: false,
            newpassage: []
          }
        },
        methods: {
          getMessage(){
            axios.get('api.php')
              .then(function (response) {
                console.log(response.data)
                app.$data.newpassage = response.data
              })
              .catch(function (error) {
                console.log(error);
              });
          }
        },
        watch: {
          newpassage: function (val) {
            // this.filteredTasks = val;
            console.log(val)
          }
        }
    }).$mount('#app');
</script>

  </body>
</html>