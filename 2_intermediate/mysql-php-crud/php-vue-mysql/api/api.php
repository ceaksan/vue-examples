<?php
  header("Content-type: application/json");
  try {
      $db = new PDO("mysql:host=db4free.net:3306;dbname=vuedb_01;charset=utf8", "vuedbuser", "vuedbproject_01");
  } catch ( PDOException $e ){
      print $e->getMessage();
  }

  $method = $_SERVER['REQUEST_METHOD'];

  function safeInput($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);

    return $data;
  }

  switch ($method) {

    case 'GET':
      
      $action = safeInput($_GET['action']);
      $id = safeInput($_GET['id']);

      // DELETE
      if($action == 'delete') {
        $query = $db->exec("DELETE FROM contacts".($id ? " where id=$id" : '')." LIMIT 1");
        if($query) {
          $json[] = [
            'message' => "Contact deleted successfully"
          ];
        } else {
          $json[] = ['error' => "Contact delete failed!"];
        }
      }

      // LIST
      else {
        $query = $db->query("SELECT * FROM contacts".($id ? " WHERE id=$id" : ''), PDO::FETCH_OBJ);
        
        if ( $query->rowCount() ){
            foreach ($query as $row) {
              $json[] = [
                'id' => $row->id,
                'name' => $row->name,
                'email' => $row->email,
                'country' => $row->country,
                'city' => $row->city,
                'job' => $row->job
              ];
            }
        }
      }

    break;

    case 'POST':
      $action = safeInput($_GET['action']);
      $id = safeInput($_POST['id']);
      $name = safeInput($_POST['name']);
      $email = safeInput($_POST['email']);
      $country = safeInput($_POST['country']);
      $city = safeInput($_POST['city']);
      $job = safeInput($_POST['job']);

      // UPDATE
      if ($action == 'update') {
        $query = $db->prepare("UPDATE contacts SET name = :name, email = :email, country = :country, city = :city, job = :job WHERE id = :id LIMIT 1");
        $query = $query->execute(['name' => $name, 'email' => $email, 'country' => $country, 'city' => $city, 'job' => $job, 'id' => $id]); 
        if($query) {
          $json[] = [
            'id' => $id,
            'name' => $name,
            'email' => $email,
            'country' => $country,
            'city' => $city,
            'job' => $job,
            'message' => "Contact edited successfully"
          ];
        } else {
          $json[] = ['error' => "Contact edit failed!"];
        }
      }
      
      // ADD
      else {
        if ($name && $email) {
          $query = $db->prepare("INSERT INTO contacts (name, email, country, city, job) VALUES (:name, :email, :country, :city, :job)");
          $query->execute(['name' => $name, 'email' => $email, 'country' => $country, 'city' => $city, 'job' => $job]);
          if($query) {
            $json[] = [
              'id' => $db->lastInsertId(),
              'name' => $name,
              'email' => $email,
              'country' => $country,
              'city' => $city,
              'job' => $job,
              'message' => "Contact added successfully"
            ];
          } else {
            $json[] = ['error' => "Contact insert failed!"];
          }
        } else{
          $json[] = ['error' => 'Error: Name and E-mail are required!'];
        }
      }
    break;

    default:
      http_response_code(404);
  }
  
  $db = null;
  
  echo json_encode($json);

  ?>